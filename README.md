# ICS Animals Test Task

This is a test task performed by Pavel 'DiPaolo' Ditenbir for ICS.

The GUI tool displays animals and provides the user the ability to filter animals in the list.

### Before Build

Please make sure you have set Qt to system path. For Linux you may do that with the following command:
```
$ export CMAKE_PREFIX_PATH=/path/to/your/Qt/installation
```

### Build

```
$ mkdir build
$ cd build
$ cmake ..
$ make
```

### Tests

```
$ cd tests
$ ./run.sh
```

### License and Copyright

MIT License

Copyright (c) 2018 Pavel 'DiPaolo' Ditenbir