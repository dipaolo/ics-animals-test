//
// MIT License
//
// Copyright (c) 2018 Pavel 'DiPaolo' Ditenbir
//
// E-mail: pavel.ditenbir@gmail.com
//
//
// Animals list model for Model/View usage
//

// own
#include "animals_model.h"

// core
#include "ics_animals_core.h"

using namespace ics::animals::gui;


AnimalsModel::AnimalsModel(QObject *parent)
    : QAbstractListModel(parent)
{
    mAnimals = ics::animals::core::GetAnimals(":/ics/animals/animals.txt");
}

int AnimalsModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return mAnimals.size();
}

QVariant AnimalsModel::data(const QModelIndex &index, int role) const
{
    QVariant retValue;

    if (!index.isValid())
        return retValue;

    const auto row = index.row();
    if (role == Qt::DisplayRole && row >= 0 && row < mAnimals.size())
        retValue = mAnimals[index.row()];

    return retValue;
}
