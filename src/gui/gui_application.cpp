//
// MIT License
//
// Copyright (c) 2018 Pavel 'DiPaolo' Ditenbir
//
// E-mail: pavel.ditenbir@gmail.com
//
//
// Own GUI Application class to store setting during runtime
//

#include "gui_application.h"

using namespace ics::animals::gui;


QApplication *app()
{
    return static_cast<QApplication*>(QCoreApplication::instance());
}


IcsAnimalsGuiApplication::IcsAnimalsGuiApplication(int &argc, char **argv) :
    QApplication(argc, argv)
{

}
