//
// MIT License
//
// Copyright (c) 2018 Pavel 'DiPaolo' Ditenbir
//
// E-mail: pavel.ditenbir@gmail.com
//
//
// Own GUI Application class to store setting during runtime
//

#pragma once

// Qt
#include <QApplication>
#include <QObject>

// core
#include "ics_animals_core.h"


namespace ics {
namespace animals {
namespace gui {


QApplication *app();


class IcsAnimalsGuiApplication : public QApplication
{
    Q_OBJECT
    Q_PROPERTY(ics::animals::core::ParamMap config READ config WRITE setConfig)

public:
    IcsAnimalsGuiApplication(int &argc, char **argv);

    ics::animals::core::ParamMap config() const { return mConfig; }
    void setConfig(const ics::animals::core::ParamMap &config) { mConfig = config; }

private:
    ics::animals::core::ParamMap mConfig;
};

}}}
