//
// MIT License
//
// Copyright (c) 2018 Pavel 'DiPaolo' Ditenbir
//
// E-mail: pavel.ditenbir@gmail.com
//
//
// Main appication window
//

// own header files
#include "main_window.h"
#include "ui_main_window.h"

// Qt
#include <QSortFilterProxyModel>

using namespace ics::animals::gui;


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    mAnimalsModel(this)
{
    ui->setupUi(this);

    QSortFilterProxyModel *proxy = new QSortFilterProxyModel(this);
    proxy->setSourceModel(&mAnimalsModel);

    ui->animalsList->setModel(proxy);

    connect(ui->searchEdit, SIGNAL(textChanged(QString)), proxy, SLOT(setFilterFixedString(QString)));
}

MainWindow::~MainWindow()
{
    delete ui;
}
