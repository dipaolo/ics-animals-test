//
// MIT License
//
// Copyright (c) 2018 Pavel 'DiPaolo' Ditenbir
//
// E-mail: pavel.ditenbir@gmail.com
//
//
// GUI application entry point
//

// Qt
#include <QApplication>

// core
#include "ics_animals_core.h"

// own
#include "gui_application.h"
#include "main_window.h"

using namespace ics::animals;


int main(int argc, char *argv[])
{
    gui::IcsAnimalsGuiApplication guiApp(argc, argv);

    QCoreApplication::setOrganizationName("DiPaolo");
    QCoreApplication::setApplicationName("ICS Animals Task");
    QCoreApplication::setApplicationVersion("0.9");

    guiApp.setConfig(core::ParseCmdline(guiApp));

    gui::MainWindow window;
    window.show();

    return guiApp.exec();
}
