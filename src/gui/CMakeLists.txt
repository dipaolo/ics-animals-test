cmake_minimum_required(VERSION 3.0.2)

project(ics_animals_test_gui)

set(CMAKE_CXX_STANDARD 11)

# Qt related
set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTORCC ON)

find_package(Qt5Widgets)

include_directories(../core)

add_executable(${PROJECT_NAME}
    "animals_model.cpp"
    "gui_application.cpp"
    "ics_animals_test.qrc"
    "main.cpp"
    "main_window.cpp"
    "main_window.ui"
)

target_link_libraries(${PROJECT_NAME}
    ics_animals_test_core
    Qt5::Widgets
)
