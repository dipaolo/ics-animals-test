//
// MIT License
//
// Copyright (c) 2018 Pavel 'DiPaolo' Ditenbir
//
// E-mail: pavel.ditenbir@gmail.com
//
//
// Animals list model for Model/View usage
//

#pragma once

#include <QAbstractListModel>


namespace ics {
namespace animals {
namespace gui {


class AnimalsModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit AnimalsModel(QObject *parent = nullptr);

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

private:
    QStringList mAnimals;
};


}}}
