//
// MIT License
//
// Copyright (c) 2018 Pavel 'DiPaolo' Ditenbir
//
// E-mail: pavel.ditenbir@gmail.com
//
//
// Main appication window
//

#pragma once

// Qt
#include <QAbstractListModel>
#include <QMainWindow>

// own
#include "animals_model.h"


namespace Ui
{
    class MainWindow;
}


namespace ics {
namespace animals {
namespace gui {


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    AnimalsModel mAnimalsModel;
};


}}}
