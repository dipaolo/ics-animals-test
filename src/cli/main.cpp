//
// MIT License
//
// Copyright (c) 2018 Pavel 'DiPaolo' Ditenbir
//
// E-mail: pavel.ditenbir@gmail.com
//
//
// CLI application entry point
//

#include <QCommandLineParser>
#include <QCoreApplication>
#include <QObject>
#include <QTranslator>


static int ParseCmdline(const QCoreApplication &app);


int main(int argc, char *argv[])
{
    QCoreApplication cliApp(argc, argv);
    QCoreApplication::setOrganizationName("DiPaolo");
    QCoreApplication::setApplicationName("ICS Animals Task");
    QCoreApplication::setApplicationVersion("0.9");

    return ParseCmdline(cliApp);
}

int ParseCmdline(const QCoreApplication &app)
{
    QCommandLineParser parser;
    parser.setApplicationDescription("ICS animals task.");

    const auto helpOption = parser.addHelpOption();
    const auto versionOption = parser.addVersionOption();
    const QCommandLineOption doNothingOption("nothing", "Do nothing.");
    parser.addOption(doNothingOption);

    parser.process(app);

    if (parser.isSet(helpOption))
        parser.showHelp();
    else if (parser.isSet(versionOption))
        parser.showVersion();

    return 0;
}
