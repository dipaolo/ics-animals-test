//
// MIT License
//
// Copyright (c) 2018 Pavel 'DiPaolo' Ditenbir
//
// E-mail: pavel.ditenbir@gmail.com
//
//
// ICS Core API functions
//

#pragma once

// Qt
#include <QCommandLineOption>
#include <QCoreApplication>
#include <QHash>
#include <QString>

// own
#include "ics_animals_core_global.h"


namespace ics {
namespace animals {
namespace core {


typedef QHash<QString, QString> ParamMap;

ICS_ANIMALS_TEST_SHARED_EXPORT ParamMap ParseCmdline(const QCoreApplication &app);
ICS_ANIMALS_TEST_SHARED_EXPORT QStringList GetAnimals(const QString &filename);


}}}
