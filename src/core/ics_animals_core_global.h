//
// MIT License
//
// Copyright (c) 2018 Pavel 'DiPaolo' Ditenbir
//
// E-mail: pavel.ditenbir@gmail.com
//
//
// Shared library helper heder file
//

#pragma once

#include <QtCore/qglobal.h>


#if defined(ICS_ANIMALS_TEST_LIBRARY)
#  define ICS_ANIMALS_TEST_SHARED_EXPORT Q_DECL_EXPORT
#else
#  define ICS_ANIMALS_TEST_SHARED_EXPORT Q_DECL_IMPORT
#endif
