//
// MIT License
//
// Copyright (c) 2018 Pavel 'DiPaolo' Ditenbir
//
// E-mail: pavel.ditenbir@gmail.com
//
//
// ICS Core API functions
//

// class/file header
#include "ics_animals_core.h"

// Qt
#include <QCommandLineParser>
#include <QFile>
#include <QLinkedList>


ics::animals::core::ParamMap ics::animals::core::ParseCmdline(const QCoreApplication &app)
{
    typedef QHash<QString, QCommandLineOption> OptionsMap;
    static OptionsMap sConfigOptions = {

        //
        // option     name     description                       available values  default value
        //

        {  "file",  { "file",  "Input file with animals list.",  "filename.txt",   "animals.txt" } }
    };


    QCommandLineParser parser;
    parser.setApplicationDescription("ICS Animals test task.");

    const auto helpOption = parser.addHelpOption();
    const auto versionOption = parser.addVersionOption();

    for (const auto &option : sConfigOptions)
    {
        parser.addOption(option);
    }

    parser.process(app);

    if (parser.isSet(helpOption))
        parser.showHelp();
    else if (parser.isSet(versionOption))
        parser.showVersion();

    ParamMap parsedParams;

    OptionsMap::const_iterator iter = sConfigOptions.cbegin();
    while (iter != sConfigOptions.cend())
    {
        parsedParams[iter.key()] = parser.value(iter.value());
        ++iter;
    }

    return parsedParams;
}

QStringList ics::animals::core::GetAnimals(const QString &filename)
{
    QStringList animals;

    QFile file(filename);
    if (file.open(QFile::ReadOnly))
    {
        auto curLine = file.readLine();
        while (!curLine.isNull())
        {
            // TODO empty lines skipping is missing;
            // easy to add

            animals.append(QString::fromStdString(curLine.toStdString()));
            curLine = file.readLine();
        }
    }
    else
    {
        // TODO provide an error here with desired mechanism (logging,
        // messgae box or whatever else)
    }

    return animals;
}
